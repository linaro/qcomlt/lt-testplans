#!/usr/bin/env python3
import argparse
import os
import sys
import itertools
from csv import DictReader
from squad_client.core.api import SquadApi
from squad_client.shortcuts import submit_results

parser = argparse.ArgumentParser()
parser.add_argument('--debug',
        action='store_true',
        dest='debug',
        help='display debug messages')
parser.add_argument('--group',
        required=True,
        help='SQUAD group slug')
parser.add_argument('--project',
        required=True,
        help='SQUAD project slug')
parser.add_argument('--build',
        required=True,
        help='Build ID within a project')
parser.add_argument('--environment',
        required=True,
        help='Filter environment in the Build')
parser.add_argument('--token',
        required=True,
        help='SQUAD authorization token')
parser.add_argument('--squad-host',
        dest='server',
        default='https://qa-reports.linaro.org/',
        help='SQUAD server')
parser.add_argument('--results',
        required=True,
        help='Test results to upload')
parser.add_argument('--limit',
        type=int,
        help='Limit amount of results in a single upload')

args = parser.parse_args()

if args.debug:
    import logging

    logging.basicConfig(level=logging.DEBUG)

tests = {}
metrics = {}
file_format = os.path.splitext(args.results)[1]

if file_format == '.csv':
    with open(args.results, 'r') as file:
        results = DictReader(file)
        for test in results:
            if test['measurement'].replace('.','',1).isdigit():
                metrics["%s/%s" % (test['name'], test['test_case_id'])] = test['measurement']
            else:
                tests["%s/%s" % (test['name'], test['test_case_id'])] = test['result']
else:
    print("Unsupported input file format: " + file_format)
    sys.exit(1)

SquadApi.configure(url=args.server, token=args.token)

print("Loaded %d tests and %d metrics" % (len(tests), len(metrics)))

if args.limit:
    tests_len = len(tests.items())
    for start, stop in zip(range(0, tests_len, args.limit), range(args.limit, tests_len + args.limit, args.limit)):
        print("Submitting tests results from %d to %d" % (start + 1, stop if stop < tests_len else tests_len))
        submit_results(group_project_slug="%s/%s" % (args.group, args.project),
                   build_version=args.build, env_slug=args.environment,
                   tests=dict(itertools.islice(tests.items(), start, stop)), metrics={})

    metrics_len = len(metrics.items())
    for start, stop in zip(range(0, metrics_len, args.limit), range(args.limit, metrics_len + args.limit, args.limit)):
        print("Submitting metrics results from %d to %d" % (start + 1, stop if stop < metrics_len else metrics_len))
        submit_results(group_project_slug="%s/%s" % (args.group, args.project),
                   build_version=args.build, env_slug=args.environment,
                   tests={}, metrics=dict(itertools.islice(metrics.items(), start, stop)))
else:
    submit_results(group_project_slug="%s/%s" % (args.group, args.project),
            build_version=args.build, env_slug=args.environment,
            tests=tests, metrics=metrics)
