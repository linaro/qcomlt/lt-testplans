#!/usr/bin/env python3
import argparse
import datetime
import jinja2
import os
import sys
from squad_client.core.api import SquadApi
from squad_client.core.models import Squad, SquadObject, TestRun, Test, ALL

parser = argparse.ArgumentParser()
parser.add_argument('--debug',
        action='store_true',
        dest='debug',
        help='display debug messages')
parser.add_argument('--group',
        dest='group',
        required=True,
        help='SQUAD group slug')
parser.add_argument('--project',
        dest='project',
        required=True,
        help='SQUAD project slug')
parser.add_argument('--build',
        dest='build',
        required=True,
        help='Build ID within a project')
parser.add_argument('--environment',
        dest='environment',
        help='Filter environment in the Build')
parser.add_argument('--token',
        dest='token',
        required=True,
        help='SQUAD authorization token')
parser.add_argument('--template',
        dest='template',
        required=True,
        help='Rendering template')
parser.add_argument('--description',
        dest='description',
        required=False,
        default=None,
        help='Test report description')
parser.add_argument('--output',
        dest='output',
        default="output",
        help='Output folder')
parser.add_argument('--squad-host',
        dest='server',
        default='https://qa-reports.linaro.org/',
        help='SQUAD server')
parser.add_argument('--disable-log-parser',
        action='store_true',
        dest='disable_log_parser',
        help='Filter out linux-log-parser suite')

args = parser.parse_args()

if args.debug:
    import logging

    logging.basicConfig(level=logging.DEBUG)

SquadApi.configure(url=args.server, token=args.token)
group = Squad().group(args.group)
project = group.project(args.project)
build = project.build(args.build)

metadata = {
    'now' : datetime.date.today().strftime("%B %d, %Y"),
    'description' : args.description,
    'env' : None,
    'manual' : None
}

if args.environment:
    envs = Squad().environments(project=project.id, slug=args.environment)
    try:
        metadata['env'] = next(iter(envs.values())).id
    except StopIteration:
        print("Warning: no environment found for %s" % args.environment)
        envs = Squad().environments(project=project.id)
        print("Valid environments are: %s" % ', '.join(env.slug for env in envs.values()))

suites = Squad().suites(project=project.id, count=-1)

# patch missing attribute
# patch for squad_client is pending upstream
for s in suites.values():
    if not hasattr(s, 'url'):
        setattr(s, 'url', "%s/api/suites/%s/" %(args.server.rstrip('/'), s.id))

# Find the manual test suite
for s in suites.values():
    if s.slug == 'manual':
        metadata['manual'] = s.url
    elif s.slug == 'linux-log-parser':
        metadata['linux-log-parser'] = s.url

testruns = Squad().testruns(build=build.id, environment=metadata['env'])
tests = Squad().tests(count=ALL, test_run__environment=metadata['env'], test_run__build=build.id)
metrics = Squad().metrics(count=ALL, test_run__environment=metadata['env'], test_run__build=build.id)

tests_automated = {}
tests_manual = {}
tests_log_parser = {}

for id, test in tests.items():
    if test.suite == metadata['manual']:
        tests_manual[id] = test
    elif args.disable_log_parser and 'linux-log-parser' in metadata and test.suite == metadata['linux-log-parser']:
        tests_log_parser[id] = test
    else:
        tests_automated[id] = test

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
template = templateEnv.get_template(args.template)

outputText = template.render(metadata=metadata, group=group, project=project, build=build,
                             suites=suites, metrics=metrics, testruns=testruns,
                             tests_automated=tests_automated, tests_manual=tests_manual,
                             server=args.server.rstrip('/'))

output = os.path.join(args.output,
                      "%s-%s-%s%s" % (args.group, args.project, args.build, os.path.splitext(args.template)[1]))

if not os.path.exists(os.path.abspath(args.output)):
    os.makedirs(os.path.abspath(args.output), mode=0o755)

with open(output, 'w') as _file:
    _file.write(outputText)

if os.path.splitext(args.template)[1] == '.textile':
    import textile
    with open("{}{}".format(os.path.splitext(output)[0], ".html"), "w") as _file:
        _file.write(textile.textile(outputText))
