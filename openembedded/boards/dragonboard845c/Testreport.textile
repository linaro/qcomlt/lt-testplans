{% set base_url = [server, '/%s/%s/' % (group.slug, project.slug)]|join %}
h1{color:blue;}. Test Report for Linaro OpenEmbedded RPB release for Qualcomm(tm) Dragonboard(tm) 845c platform

!{width: 20%; padding: 5px; }https://www.linaro.org/assets/images/Linaro-Logo.svg(Linaro Logo)!

*Date:* {{ metadata.now }}

<hr>

h1. Build information

This test report is generated from the test data available on the following build location:

"{{ base_url + 'build/' + build.version + '/' }}":{{ base_url + 'build/' + build.version + '/' }}

h1. Test Summary

{% if testruns.values()|selectattr('job_url')|list|length %}
The following LAVA jobs were executed:

table{table-layout: fixed; width: 50%; border:1px solid black; border-spacing: 10px; text-align: left; border-collapse:collapse}.
|:. 100| 20| 60| 20|
{background:#ddd}. |_. Job ID |_. Job URL |_. Job Status |
{%- for tr in testruns.values() %}
| {{ tr.job_id }} | "{{ tr.job_url }}":{{ tr.job_url }} | {{ tr.job_status }} |
{%- endfor %}
{% endif %}

{%- set pass= tests_automated.values()|selectattr('status', 'equalto', 'pass')|list|length %}
{%- set fail= tests_automated.values()|selectattr('status', 'equalto', 'fail')|list|length %}
{%- set skip= tests_automated.values()|selectattr('status', 'equalto', 'skip')|list|length %}
{%- set benchmarks= metrics.values()|list|length %}
{%- set automated = pass+fail+skip+benchmarks %}

{% if automated > 0 %}
table{width: 25%; border:1px solid black; border-spacing: 10px; text-align: left; border-collapse:collapse}.
{background:#ddd}. |_. Pass |_. Fail |_. Skip |_. Benchmarks|_. Total |
| {{pass}} | {{fail}} | {{skip}} | {{benchmarks}} | {{automated}} |
{% else %}
None.
{% endif %}

h1. Test Results

{% if automated %}
{% for suite, _tests in tests_automated.values()|groupby('suite') %}
h3. {{ loop.index }}. {{ suites.values() | selectattr('url', 'equalto', suite) | first | attr('slug')}}

_Summary_
{%- set pass= _tests|selectattr('status', 'equalto', 'pass')|list|length %}
{%- set fail= _tests|selectattr('status', 'equalto', 'fail')|list|length %}
{%- set skip= _tests|selectattr('status', 'equalto', 'skip')|list|length %}

table{width: 10%; border:1px solid black; border-spacing: 10px; text-align: left; border-collapse:collapse}.
{background:#ddd}. |_. Result |_. |
 | Pass  | {{pass}} |
 | Fail  | {{fail}} |
 | Skip  | {{skip}} |
{background:#ddd}.  | Total | {{ pass + fail + skip}} |

_Tests details_

table{table-layout: fixed; width: 50%; border:1px solid black; border-spacing: 10px; text-align: left; border-collapse:collapse}.
|:. 100| 80| 20|
{background:#ddd}. |_. Test Case |_. Result |
    {%- for test in _tests %}
        | {{ test.short_name }} | {{ test.status }} |
    {%- endfor %}
{% endfor %}
{% else %}
None.
{% endif %}

h1. Benchmarks Results

{% if metrics.values()|list|length %}
{% for suite, _tests in metrics.values()|groupby('suite') %}
h3. {{ loop.index }}. {{ suites.values() | selectattr('url', 'equalto', suite) | first | attr('slug')}}

table{table-layout: fixed; width: 50%; border:1px solid black; border-spacing: 10px; text-align: left; border-collapse:collapse}.
|:. 100| 80| 20|
{background:#ddd}. |_. Test Case |_. Result |
    {%- for test in _tests %}
        | {{ test.short_name }} | {{ test.result }} |
    {%- endfor %}

{% endfor %}
{% else %}
None.
{% endif %}
