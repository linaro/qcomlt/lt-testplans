#!/bin/bash

testplan2html.py -s \
		 -f "${1-Testplan.yaml}" \
		 --testplan-template-name "${2-Testplan.textile}" \
		 --templates-directory $(pwd) \
		 --output $(pwd)
